﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T data)
        {
            var temp = Data.ToList();
            temp.Add(data);
            this.Data = temp;
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == data.Id));
        }

        public Task UpdateAsync(T data)
        {
            var temp = Data.ToList();
            var employee = Data.FirstOrDefault(x => x.Id == data.Id);
            temp.Remove(employee);
            temp.Add(data);
            this.Data = temp;
            return Task.CompletedTask;
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var temp = Data.ToList();
            var employee = Data.FirstOrDefault(x => x.Id == id);
            temp.Remove(employee);
            this.Data = temp;
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id) == null);
        }
    }
}